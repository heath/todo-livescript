This is an example todo app based on the [TODOMVC project](http://todomvc.com/). This project uses various technologies not compliant with the TODOMVC spec, so it's not a fork. Nevertheless, here's a reference livescript app the way it should be built :)


Technologies primarily used: 

  - angular
  - browserify
  - closure
  - gulp
  - jade
  - js-csp
  - livescript
  - mori
  - stylus
  - sweet.js
  - uirouter


Goals:

  - Create a working todo app similar to TODOMVC
  - Implement undo and redo
  - It may also be nice to introduce sessions


Type this into a terminal to launch the application:

    npm install -g gulp cult
    npm install
    cult
