gulp = require "gulp"
rs   = require "run-sequence"

clean      = require "gulp-clean"
livescript = require "gulp-livescript"
concat     = require "gulp-concat"
jade       = require "gulp-jade"
serve      = require "gulp-serve"
stylus     = require "gulp-stylus"

gulp.task "clean", ->
  gulp.src "build/", read: false
      .pipe clean()

gulp.task "stylus", ->
  gulp.src "app/styles/app.styl"
      .pipe stylus
        compress: true
      .pipe gulp.dest "build/styles"

gulp.task "jade", ->
  gulp.src "app/index.jade"
      .pipe jade()
      .pipe gulp.dest "build/"

  gulp.src "app/scripts/**/*.jade"
      .pipe jade()
      .pipe gulp.dest "build/"

gulp.task "livescript", ->
  gulp.src <[
    app/scripts/app.ls
    app/scripts/components/**/*.ls
    app/scripts/routes.ls
    app/scripts/bootstrap.ls
  ]>
  .pipe livescript bare: true
  .pipe concat "app.js"
  .pipe gulp.dest "build/"

gulp.task "watch", ->
  gulp.watch "app/**/*", { read: false }, <[ build ]>

gulp.task "serve", serve
  root: <[ build ]>
  port: 9000

gulp.task "default", <[
  serve
  watch
  build
]>

gulp.task "build", ->
  rs "clean", <[
    stylus
    jade
    livescript
  ]>
