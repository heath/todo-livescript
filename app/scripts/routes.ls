app = angular.module "app"

app.config [
  "$stateProvider", "$urlRouterProvider",
  ($stateProvider, $urlRouterProvider) ->

    $stateProvider
      .state "todo",
        url: "/"
        controller: "TodoCtrl"
        templateUrl: "/components/todo/index.html"

      $urlRouterProvider.otherwise "/"
]
.run [ '$rootScope', '$state', '$stateParams', ($rootScope, $state, $stateParams) ->
  $rootScope.$state = $state
  $rootScope.$stateParams = $stateParams
]
