#global angular

/*
 * Directive which executes an expression when its element emits
 * an `escape` keydown event.
*/

angular.module("todomvc").directive "todoEscape", !->

  ESCAPE_KEY = 27

  (scope, elem, attrs) ->
    elem.bind "keydown", (event) !->
      scope.$apply attrs.todoEscape if event.keyCode is ESCAPE_KEY
