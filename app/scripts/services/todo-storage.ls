# global angular 

/*
 * service which persists and retrieves TODOs from localStorage
*/

angular.module("app").factory "todoStorage", ->

  STORAGE_ID = "todos"

  get: !->
    JSON.parse localStorage.getItem STORAGE_ID or "[]"

  put: (todos) !->
    localStorage.setItem STORAGE_ID, JSON.stringify todos
