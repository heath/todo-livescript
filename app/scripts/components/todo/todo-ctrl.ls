#global angular 

/*
 * The main controller for the app. The controller:
 * - retrieves and persists the model via the todoStorage service
 * - exposes the model to the template and provides event handlers
*/

app = angular.module "app"

app.controller "todoCtrl", [
  "$scope", "$state", "$filter", "todoStorage"
  ($scope, $state, $filter, todoStorage) !->

    todos = $scope.todos = todoStorage.get()
    $scope.newTodo = ""
    $scope.editedTodo = null
    $scope.$watch "todos", (newValue, oldValue) !->
      $scope.remainingCount = $filter("filter")(todos,
        completed: false
      ).length
      $scope.completedCount = todos.length - $scope.remainingCount
      $scope.allChecked = not $scope.remainingCount
      # This prevents unneeded calls to the local storage
      todoStorage.put todos  if newValue isnt oldValue
    , true

    # Monitor the current route for changes and adjust the filter accordingly.
    $scope.$on "$routeChangeSuccess", !->
      status = $scope.status = $routeParams.status or ""
      $scope.statusFilter = (if (status is "active") then completed: false else (if (status is "completed") then completed: true else null))

    $scope.addTodo = !->
      newTodo = $scope.newTodo.trim()
      return unless newTodo.length
      todos.push
        completed : false
        title     : newTodo

      $scope.newTodo = ""

    $scope.editTodo = (todo) !->
      $scope.editedTodo = todo

      # Clone the original todo to restore it on demand.
      $scope.originalTodo = angular.extend({}, todo)

    $scope.doneEditing = (todo) !->
      $scope.editedTodo = null
      todo.title = todo.title.trim()
      $scope.removeTodo todo  unless todo.title

    $scope.revertEditing = (todo) !->
      todos[todos.indexOf(todo)] = $scope.originalTodo
      $scope.doneEditing $scope.originalTodo

    $scope.removeTodo = (todo) !->
      todos.splice todos.indexOf(todo), 1

    $scope.clearCompletedTodos = !->
      $scope.todos = todos = todos.filter (val) ->
        not val.completed

    $scope.markAll = (completed) !->
      todos.forEach (todo) ->
        todo.completed = not completed

]
